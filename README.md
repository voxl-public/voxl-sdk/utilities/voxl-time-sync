# voxl-time-sync

This helper utility will run until the VOXL's date and time are automatically set by the internet.

This is required in order for some programs to execute correctly, such as OpenVPN.

## Building

Start the voxl-emulator docker

```
# Create voxl-time-sync ipk
./make_package.sh
```

## Installation

```
# Install onto VOXL
./install_on_voxl.sh
```

## Usage

```
voxl-time-sync
```

Will run and block until the VOXL's date and time are automatically set by the internet.
